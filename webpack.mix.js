/**
 * Bootstrap Boilerplate
 */

const mx = require("laravel-mix");

mx.setPublicPath('assets');

// Generate source maps when not in production
if (!mx.inProduction()) {
  mx.webpackConfig({devtool: "source-map"})
    .sourceMaps()
}

// Cache busting
// mx.version();

// Copy fontawesome assets
mx.copy("node_modules/@fortawesome/fontawesome-free/webfonts", "assets/fonts");
// mx.copy("resources/img/chosen-sprite.png", "assets/img");
// mx.copy("resources/img/chosen-sprite@2x.png", "assets/img");

// Compile
mx.js("resources/js/app.js", "assets/js")
  .sass("resources/sass/app.scss", "assets/css");
