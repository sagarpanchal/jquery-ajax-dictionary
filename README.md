# Setup Instructions

```
npm i   # to install dependencies
```

### ESLint
```
npm i -g eslint
```

### ESLint plugins for vscode
```
code --install-extension dbaeumer.vscode-eslint
```

### NPM scripts
```
npm run watch   # for live updates while testing,
npm run dev     # for development build and
npm run prod    # for production build.
```
