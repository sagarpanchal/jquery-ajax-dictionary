'use strict';

document.addEventListener("DOMContentLoaded", function() {
  $("#meanings-card-body").collapse('hide');
  $("#table-1 tbody").html('');
  $.notifyClose();
  $.notify(
    { message: 'Type a keyword to retrieve meanings' },
    { type: 'secondary', allow_duplicates: false }
  );

  var word = document.getElementById('word');
  $(word).focus();

  var wordRegEx = /^[A-Za-z0-9\ ]*$/;
  var wordMask = new IMask(word, { mask: wordRegEx });

  var wordComplete = new Awesomplete(word, { minChars: 2, autoFirst: true });

  var wordSearchTimer;
  var wordDataListTimer;
  var wordPrevValue;
  $(word).on('input awesomplete-selectcomplete', function (e) {
    console.log(e.type);
    wordComplete.evaluate();
    if (e.type === "awesomplete-selectcomplete") {
      wordComplete.close();
    }
    if (word.value !== '' || word.value != wordPrevValue) {
      clearTimeout(wordSearchTimer);
      clearTimeout(wordDataListTimer);
      if ((e.type !== "awesomplete-selectcomplete")) {
        wordDataListTimer = setTimeout(function () {
          spellCheck(word.value, wordComplete);
        }, 500);
      }
      wordSearchTimer = setTimeout(function () {
        updateDictionary(word.value);
        wordPrevValue = word.value;
      }, 1000);
    }
    if (word.value === '') {
      $("#meanings-card-body").collapse('hide');
      $("#table-1 tbody").html('');
      wordComplete.close();
    }
  });
});

/**
* Get data from API
* @param {string} phrase (required) phrase to be translated,
* values: arbitrary text, no default, case sensitive
*/
function updateDictionary(phrase) {
  var url = "https://cors-anywhere.herokuapp.com/https://api.datamuse.com/words?sl=" + phrase + "&md=d";
  $.ajax({
    url: url,
    dataType: "json",
    success: (result) => {
      var tmp = result;
      $("#table-1 tbody").html('');
      fillTable(result, phrase);
    }
  })
}

/**
* Check spellings of the word
* @param {string} word word to check
* @param {*} awesomplete Awesomplete instance
*/
function spellCheck(word, awesomplete) {
  $.ajax({
    url: "https://cors-anywhere.herokuapp.com/https://api.datamuse.com/sug?s=" + word,
    dataType: "json",
    success: (result) => {
      var tmp = result;
      if (result != null) {
        var list = new Array();
        for (var i in result) {
          list.push(result[i]["word"]);
        }
        awesomplete.list = list;
      }
    }
  })
}

/**
* Update Table
* @param {object} input JSON response
*/
function fillTable(input, phrase) {
  $.notifyClose();
  if (typeof input[0] != 'undefined') {
    if (phrase == input[0]["word"] && typeof input[0]["defs"] != 'undefined') {
      var meanings = input[0]["defs"];
      if (typeof meanings !== "undefined") {
        $("#meanings-card-body").collapse('show');
        for (var i in meanings) {
          var tmp = _.unescape(
            "&lt;strong&gt;" + meanings[i].
            replace(/(\t)/gm, ".&lt;/strong&gt; &mdash; ")
          );
          $("#table-1 tbody").append("<tr><td>" + tmp + "</td></tr>");
        }
      } else {
        $.notify(
          { message: "Couldn't retrieve any meanings!" },
          { type: 'danger', allow_duplicates: false }
        );
      }
    } else {
      $.notify(
        { message: "Couldn't retrieve any meanings!" },
        { type: 'danger', allow_duplicates: false }
      );
    }
  }
}
